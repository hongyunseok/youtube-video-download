var express = require('express');
var ytdl = require('youtube-dl');

var app = express();


app.listen(3000);
app.use(express.static('src/public'));
app.set('view engine', 'pug');
app.set('views', './src/views');

app.get('/', (req, res) => {
	res.render('index', {
		req: req
	});
});

app.get('/about', (req, res) => {
	res.render('about');
});

app.get('/price', (req, res) => {
	res.render('price');
});

app.get('/video', (req, res) => {
	if(req.query.url && (req.query.url.startsWith('https://www.youtube.com/watch?v=') || req.query.url.startsWith('https://youtu.be/'))) {
		ytdl.getInfo(req.query.url, '', (err, info) => {
			if (err) {
				res.redirect('/?status=failed');
			} else {
				res.redirect(info.url);
			}
		});
	} else {
		res.redirect('/?status=failed');
	}
});

app.get('/developers', (req, res) => {
	res.render('developers');
});