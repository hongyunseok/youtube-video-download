function onEnter() {
	var search = document.getElementsByName('search')[0];
	if(search.value.startsWith('https://www.youtube.com/watch?v=')
		|| search.value.startsWith('https://youtu.be/')) {
		setStatus('#32d296', '잠시만 기달려주세요!', 0);
		window.location = '/video?url=' + search.value;
	} else {
		setStatus('#f0506e', '올바른 URL 형식이 아닙니다!', 2000);
		search.value = '';

	}
}	

function setStatus(color, text, time) {
	var status = document.getElementsByName('status')[0];
	status.textContent = text;
	status.style.color = color;
	if(time !== 0) {
		setTimeout(() => {
			setStatus('#ffffff', '원하는 영상의 URL를 입력해주세요!');
		}, time);
	}
}

document.addEventListener('keydown', function(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
		onEnter();
    }
}, true);